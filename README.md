## Dithrax_test

This repository contains code to test `model.save_weights`, which does not work
properly.

Requirements:
* Tensorflow 2.15.1 or inferior. It should work also with Tensorflow 2.16.1, if
you install `pip install tf_keras` and, before running
the script, one exports `export TF_USE_LEGACY_KERAS=1`
* tqdm

To do:
* download the repository
* go to `/scripts`
* run `python main_train.py` (this trains a model 
and save it in the same directory)
* run `python main_test_weights.py` (this loads the same model and the
previously saved weights and runs `model.evaluate`: the results are however
**poor**, thus providing evidence that there is a problem)

Considerations:
* It may be that the problem is due to the custom loss functions and/or
metrics, which require different parameters for the outputs (this is a 
multi-output model)

