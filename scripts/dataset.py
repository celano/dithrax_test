import numpy as np
from collections import Counter
import os
import copy
import chuliu_edmonds
from tqdm import tqdm
import tensorflow as tf
import random
#import fasttext.util
#import fasttext

class Dataset:
  train_max_sent_length = None
  train_max_dep_distance = None
  train_max_form_length = None
  train_max_lemma_length = None
  train_max_or_text_length = None # or_text is original text, without
                                  # any split (tokenization 
                                  # and sentence split)
  train_max_tokenized_length = None # tokenized is the text after splitting
                                    # for tokens and sentences
  train_dicts = {}

  def __init__(self, paths, kind, task):
    self.paths = paths
    self.kind = kind
    self.task = task
    self.import_conllu() # generates self.conllu
    self.data = [] # contains the main data for splitter or parser based on
                   # self.conllu

  @property
  def paths(self):
   return self._paths

  @paths.setter
  def paths(self, value):
    if isinstance(value, list) or isinstance(value, tuple):
      self._paths = value
    else:
      raise Exception("The argument 'paths' is a list or tuple of paths")

  @property
  def kind(self):
   return self._kind

  @kind.setter
  def kind(self, value):
    if value in ("train", "dev", "test"):
      self._kind = value
    else:
      raise Exception("The argument 'kind' only accepts "\
                      "the values 'train', 'dev', or 'test'")

  @property
  def task(self):
    return self._task

  @task.setter
  def task(self, value):
    if value in ("splitter", "parser"):
      self._task = value
    else:
      raise Exception("The argument 'task' only accepts "\
                      "the values 'splitter' and 'parser'")

  def import_conllu(self):
    data = []
    for p in self.paths:
      with open (p) as f:
        d_ = f.read()
        data.append(d_)
    data = "".join(data)
    data = data.rstrip() # delete last \n\n
    sentences = data.split("\n\n")
    self.conllu = []
    for s in tqdm(sentences):
      words = s.split("\n")
      sent = []
      fields = filter(lambda x: not x.startswith("#"), words)
      for w in fields:
        fields_ = filter(lambda x: x.startswith("#"), words)
        word = dict()
        for q in fields_:
          sd = q.split(" = ")
          word[sd[0].replace("# ", "")] = sd[1]
        lines = w.split("\t")
        for n, f in enumerate(lines):
          if n == 0:
            word["id"]     = f
          elif n == 1:
            word["form"]   = f
          elif n == 2:
            word["lemma"]  = f
          elif n == 3:
            word["upos"]   = f
          elif n == 4:
            word["xpos"]   = f
          elif n == 5:
            word["feats"]  = f
          elif n == 6:
            word["head"]   = f
          elif n == 7:
            word["deprel"] = f
          elif n == 8:
            word["deps"]   = f
          elif n == 9:
            word["misc"]   = f
        sent.append(word)
      self.conllu.append(sent)
    print("### The file has been imported into "\
          "the instance variable .conllu ###") 

  def find_optimal_sent_length(self, percentage):
    assert 0 < percentage <= 1, "The percentage must be > 0 and < 1"
    # optimal sent length is calculated on self._data_for_splitter for
    # task "splitter" because punctuation marks are attached
    # to previous tokens
    dataset = self.conllu if self.task == "parser"\
                          else self._data_for_splitter
    if self.task == "parser":
      l_ = []
      for x in dataset:
        count = 0
        for y in x:
          # discard ellipsis and MWE
          if any([char in y["id"] for char in [".", "-"]]):
             pass
          else:
             count += 1
        l_.append(count)
      counter = Counter(l_)
      sorted_counter = sorted(counter.items(), key=lambda x:x[1],
                                               reverse=True)
    else:
      # for the task "splitter", elliptical and MWE tokens have already 
      # been filtered in self._data_for_splitter
      counter = Counter([len(x) for x in dataset])
      sorted_counter = sorted(counter.items(), key=lambda x:x[1],
                                               reverse=True)
    c = 0
    length = sum(counter.values())
    for n1, x in enumerate(sorted_counter):
      c += x[1]
      if c / length >= percentage:
        # the following osl is to make sure that the highest length is
        # selected, because the order is according to
        # the frequency of lengths
        osl = max(x[0] for x in sorted_counter[0:n1 + 1])
        self.optimal_sent_length = osl # x[0]
        self.counter_sent_length = sorted_counter
        break
    s_ = "### The sentence length selected is "\
         "*** {l} *** (i.e., {per} of all sentences are as long as {l} words"\
         " or shorter) "\
         "and can be accessed at the instance variable "\
         ".optimal_sent_length,"\
         " while sentence length statistics can be accessed at the"\
         " instance variable "\
         ".counter_sent_length ###".format(l=self.optimal_sent_length,
                                           per=percentage)
    print(s_)

  def find_optimal_dep_distance(self, percentage):
    assert 0 < percentage <= 1, "The percentage must be > 0 and < 1"
    # ellipical nodes and MWE have head = "_", so they are filtered
    # in the following
    counter = Counter([abs(int(y["head"]) -int(y["id"])) 
                       for x in self.conllu for y in x if y["head"] != "_"])
    sorted_counter = sorted(counter.items(), key=lambda x:x[1], reverse=True)
    c = 0
    length = sum(counter.values())
    for n1, x in enumerate(sorted_counter):
      c += x[1]
      if c / length >= percentage:
        # the following osl is to make sure that the highest dep distance is
        # selected, because the order is according to
        # the frequency of dep distances
        osl = max(x[0] for x in sorted_counter[0:n1 + 1])
        self.optimal_dep_distance = osl # x[0]
        self.counter_dep_distance = sorted_counter
        break
    s_ = "### The dependency length selected "\
         "is *** {l} *** "\
         "(i.e., {per} of all dep distances are as long as {l} "\
         "or shorter) "\
         "and can be accessed at the instance variable "\
         ".optimal_dep_distance, while dependency length statistics can "\
         "be accessed at the instance variable .counter_dep_distance "\
         "###".format(l=self.optimal_dep_distance,
                      per=percentage)
    print(s_)

  # the following is used for the fields form, lemma, or_text, tokenized
  def find_optimal_word_length(self, percentage, kind):
    assert 0 < percentage <= 1, "The percentage must be > 0 and < 1"
    assert self.data, "The self.data variable is empty: the methods"\
                      "split_sents_for_splitter "\
                      "or split_sents_add_dep_distance must be called first"
    counter = Counter([len(y[kind]) for x in self.data for y in x])
    sorted_counter = sorted(counter.items(), key=lambda x:x[1], reverse=True)
    c = 0
    length = sum(counter.values())
    for n1, x in enumerate(sorted_counter):
      c += x[1]
      if c / length >= percentage:
        # the following osl is to make sure that the highest length is
        # selected, because the order is according to
        # the frequency of lengths
        osl = max(x[0] for x in sorted_counter[0:n1 + 1])
        setattr(self, "optimal_" + kind + "_length", osl) # x[0]
        setattr(self, "counter_" + kind + "_length", sorted_counter)
        break
    s_ = "### The optimal {t} length selected is "\
         "*** {v} *** "\
         "(i.e., {per} of all {t} is as long as {v} characters or shorter) "\
         "and can be accessed at the instance variable "\
         ".optimal_{t}_length,"\
         " while word length statistics can be accessed at "\
         "the instance variable "\
         ".counter_{t}_length ###".format(v=getattr(self, 
                                   "optimal_" + kind + "_length"), t=kind,
                                   per=percentage)
    print(s_)

  def find_dep_distance(self, head_value, id_value):
    return head_value - id_value if head_value != 0 else 0

  def reduce_dep_distance(self, distance, max_dep_dist):
    return max(-max_dep_dist, min(max_dep_dist, distance))

  def split_sents_add_dep_distance(self, 
                                   max_length=None, 
                                   max_dep_distance=None,
                                   percentage_sl=0.99,
                                   percentage_dd=0.99):
    if (self.kind in ("dev", "test") 
          and Dataset.train_max_sent_length == None):
       raise Exception("This method needs to be called first on the "\
                       "train set, so that max sent and word lengths "\
                       "can be calculated!")
    elif (self.kind in ("dev", "test") 
          and Dataset.train_max_sent_length != None):
       print("### For dev and test sets, max_length is set to "\
             "Dataset.train_max_sent_length ###")
    elif (self.kind == "train" and max_length == None):
       self.find_optimal_sent_length(percentage_sl)
       Dataset.train_max_sent_length = self.optimal_sent_length
    elif self.kind == "train" and max_length != None:
       Dataset.train_max_sent_length = max_length

    if (self.kind in ("dev", "test") 
          and Dataset.train_max_dep_distance == None):
       raise Exception("This method needs to be called first on the "\
                       "train set, so that max dependency distance "\
                       "can be calculated!")
    elif (self.kind in ("dev", "test") 
          and Dataset.train_max_dep_distance != None):
       print("### For dev and test sets, max_dep_distance is set to "\
             "Dataset.train_dep_distance ###")
    elif (self.kind == "train" and max_dep_distance == None):
       self.find_optimal_dep_distance(percentage_dd)
       Dataset.train_max_dep_distance = self.optimal_dep_distance
    elif self.kind == "train" and max_dep_distance != None:
       Dataset.train_max_dep_distance = max_dep_distance

    self.data = []
    self.original_sent_ids = []
    for n, x in enumerate(tqdm(self.conllu)):
        s_ = []
        if len(x) <= Dataset.train_max_sent_length:
          for c in x:
            # the following skips ellipsis and MWE 
            if any([char in c["id"] for char in [".", "-"]]):
              pass
            else:
              di = dict(c)
              dep_dist = self.find_dep_distance(int(di["head"]),int(di["id"]))
              di["dep_distance"] = self.reduce_dep_distance(dep_dist, 
                                               Dataset.train_max_dep_distance)
              s_.append(di)
          self.data.append(s_)
          self.original_sent_ids.append(n)
        else:
         new_sents = [x[i:i+Dataset.train_max_sent_length] for i in range(0,
                                       len(x), Dataset.train_max_sent_length)]
         for d in new_sents:
           s_ = []
           for c in d:
             # the following skips ellipsis and MWE 
             if any([char in c["id"] for char in [".", "-"]]):
               pass
             else:
               di = dict(c)
               dep_dist = self.find_dep_distance(int(di["head"]),
                                                 int(di["id"]))
               di["dep_distance"] = self.reduce_dep_distance(dep_dist,
                                               Dataset.train_max_dep_distance)
               s_.append(di)
           self.original_sent_ids.append(n)
           self.data.append(s_)

  def create_position_indeces(self):
    y_ = [[int(y["id"]) for y in x ] for x in self.data]

    self.position_indices = tf.keras.utils.pad_sequences(y_, padding="post",
            maxlen=Dataset.train_max_sent_length, truncating="post", 
            value=0, dtype="int32")
    self.max_position = np.max(self.position_indices)

#  def create_head_dependents(self):
#    data_ = np.full((len(self.data), Dataset.train_max_sent_length,
#                                     Dataset.train_max_sent_length), -1, 
#                                                              dtype="float64")
#    for n1, x in enumerate(self.data):
#      for n2, y in enumerate(x):
#        for n3, z in enumerate(x):
#          if z["head"] == y["id"]:
#            data_[n1, n2, n3] = 1.
#          else:
#            data_[n1, n2, n3] = 0.
#    self.dependents = data_
#    self.head = np.transpose(self.dependents)

  def create_char_dict(self, kind):
    assert kind in ("form", "lemma", "or_text", "tokenized"), "the argument "\
               "'kind' only accepts 'form','lemma','or_text', and 'tokenized'"
    if self.kind == "train":
      d_ = {}
      all_forms = ["".join(x) for x in map(lambda x: [y[kind] for y in x], 
                                           self.data)]
      all_char = set([y for x in all_forms for y in x])
      dict_char_num = dict(zip(all_char, range(1, len(all_char) + 1)))
      dict_char_num["[PAD]"] = 0
      dict_char_num["[x]"] = len(all_char) + 1
      dict_char_num["[end]"] = len(all_char) + 2
      dict_char_num = {k: v for k, v in sorted(dict_char_num.items(), 
                                               key=lambda item: item[1])}
      dict_num_char = {v: k for k, v in dict_char_num.items()}
      d_["char_num"] = dict_char_num
      d_["num_char"] = dict_num_char
      d_["char_length"] = len(dict_char_num)
      Dataset.train_dicts[kind] = d_
    elif self.kind in ("dev", "test"):
      if Dataset.train_dicts[kind] != None: 
        print("### Dictionaries have to be "\
              "created only for the train dataset, "\
              "so Dataset.train_dicts will be used ###")
      else:
        raise Exception("This method should first be called on the train set")

  def split_words(self, kind, max_word_length=None):
    assert kind in ("form", "lemma", "or_text", "tokenized"), "The argument\
             'kind' only accepts 'form', 'lemma', 'or_text', and 'tokenized'"
    if (self.kind in ("dev", "test") 
          and getattr(Dataset, "train_max_" + kind + "_length") == None):
       raise Exception("This method needs to be called first on the "\
                       "train set, so that max word length can " \
                       "be calculated!")
    elif (self.kind in ("dev", "test") 
          and getattr(Dataset, "train_max_" + kind + "_length") != None):
       print("### For dev and test sets, max_word_length is set to "\
             "Dataset.train_max_{}_length ###".format(kind))
    elif (self.kind == "train" and max_word_length == None):
       self.find_optimal_word_length(0.99, kind)

       if kind in ("form", "or_text"):
         setattr(Dataset, "train_max_" + kind + "_length", getattr(self, 
                                             "optimal_" + kind + "_length"))
       else: # +1 to account for the final marker [end] in lemma and tokenized
         setattr(Dataset, "train_max_" + kind + "_length", getattr(self, 
                                          "optimal_" + kind + "_length") + 1)

    elif self.kind == "train" and max_word_length != None:
       setattr(Dataset, "train_max_" + kind + "_length", max_word_length)

    assert Dataset.train_dicts[kind] != None, "create_char_dict() must "\
                                              "first be called"
    data_ = np.zeros((len(self.data), Dataset.train_max_sent_length, 
               getattr(self, "train_max_" + kind + "_length")), dtype="int32")
    for n0, sent in enumerate(tqdm(self.data)):
      for n1, word in enumerate(sent):
        for n2, char in enumerate(word[kind]):
          if kind in ("form", "or_text") and n2 < getattr(self, 
                                        "train_max_" + kind + "_length") - 1:
            if char in Dataset.train_dicts[kind]["char_num"].keys():
              data_[n0,n1,n2] = Dataset.train_dicts[kind]["char_num"][char]
            else:
              data_[n0,n1,n2] = Dataset.train_dicts[kind].get("char_num"
                                                        ).get("[x]")
          elif kind in ("lemma", "tokenized") and n2 < getattr(
                                   self, "train_max_" + kind + "_length") - 1:
            if char in Dataset.train_dicts[kind]["char_num"].keys():
              data_[n0,n1,n2] = Dataset.train_dicts[kind]["char_num"][char]
            else:
              data_[n0,n1,n2] = Dataset.train_dicts[kind].get("char_num"
                                                        ).get("[x]")
        if kind in ("lemma", "tokenized"):
          le_ = min(len(word[kind]), getattr(self, 
                                        "train_max_" + kind + "_length") -1)
          data_[n0, n1,le_] = Dataset.train_dicts[kind]["char_num"]["[end]"]
    if kind == "form":
      self.forms = data_
      self.sent_length =[len([1 for y in x if np.any(y)]) for x in data_]
      print("### The data can be accessed at .forms ###")
      print("### Length of each sentence is in .sent_length ###")
    elif kind == "lemma":
      self.lemmas = data_
      print("### The data can be accessed at .lemmas ###")
    elif kind == "or_text":
      self.or_text = data_
      print("### The data can be accessed at .or_text ###")
    elif kind == "tokenized":
      self.tokenized = data_
      print("### The data can be accessed at .tokenized ###")

  def vectorize_x_fasttext(self, language):
    fasttext.util.download_model(language, if_exists='ignore')
    ft = fasttext.load_model("cc." + language + ".300.bin")
    sents = [[y["form"] for y in x] for x in self.data]
    matrix = np.zeros((len(self.data), Dataset.train_max_sent_length, 300))
    for n1, x in enumerate(sents):
      for n2, y in enumerate(x):
        matrix[n1, n2] = ft.get_word_vector(y)
    self.fasttext_vectors = matrix

  def create_label_dicts(self, kind):
    assert kind in ("upos", "deprel", "feats", "xpos"),\
                                         "The argument 'kind' only accepts "\
                                       "'upos' or 'deprel'"
    if self.kind == "train":
      d_ = {}
      labels = [[y[kind] for y in x] for x in self.data]
      type_labels = set(y for x in labels for y in x)
      type_numbers = range(len(type_labels))
      dict_type_num = dict(zip(type_labels, type_numbers))
      dict_type_num["[X]"] = len(dict_type_num)
      dict_type_num = {k: v for k, v in sorted(dict_type_num.items(), 
                                               key=lambda item: item[1])}
      dict_num_type =  {v: k for k, v in dict_type_num.items()}
      d_["type_num"] = dict_type_num
      d_["num_type"] = dict_num_type
      d_["length"] = len(dict_type_num)
      Dataset.train_dicts[kind] = d_
    elif self.kind in ("dev", "test"):
      if Dataset.train_dicts[kind] != None: 
        print("### Dictionaries have to be"\
              "created only for the train dataset, "\
              "so Dataset.train_dicts will be used ###")
      else:
        raise Exception("This method should first be called on the train set")

  def prepare_y(self, kind):
    assert kind in ("upos", "deprel", "feats", "xpos"),\
                                          "the argument 'kind' only accepts "\
                                       "'upos' or 'deprel'"
    labels = [[y[kind] for y in x] for x in tqdm(self.data)]
    y_ = [[Dataset.train_dicts[kind]["type_num"][y] 
             if y in Dataset.train_dicts[kind]["type_num"].keys() 
             else Dataset.train_dicts[kind]["type_num"]["[X]"]
           for y in x] 
          for x in labels]
    setattr(self, kind, tf.keras.utils.pad_sequences(y_, padding="post",
            maxlen=Dataset.train_max_sent_length, truncating="post", 
            value=-1, dtype="int32"))
    s_ = "### The data can be accessed at .{} ###".format(kind)
    print(s_)

  def prepare_y_dep_distance(self):
    dist = [[int(y["dep_distance"]) for y in x] for x in self.data]
    sents = []
    for x in tqdm(dist):
      s =[]
      for y in x:
        if y < 0 and y != 0:
          s.append(abs(y) + Dataset.train_max_dep_distance)
        else:
          s.append(y)
      sents.append(s)
    self.dep_distance = tf.keras.utils.pad_sequences(sents,
                   maxlen=Dataset.train_max_sent_length,
                   padding="post", truncating="post",
                   value=-1, dtype="int32")
    print("### The data can be accessed at .dep_distance ###")

  def prepare_y_head(self):
    head = [[int(y["head"]) for y in x] for x in self.data]
    for n1, x in enumerate(head):
      for n2, y in enumerate(x):
         if int(y) >= Dataset.train_max_sent_length:
           x[n2] = Dataset.train_max_sent_length - 1
#        while int(y) > Dataset.train_max_sent_length - 1:
#          y = int(y) - Dataset.train_max_sent_length
#          x[n2] = y
    head = [[-1] + x for x in head]
    self.head = tf.keras.utils.pad_sequences(head, padding="post",
            maxlen=Dataset.train_max_sent_length +1, truncating="post", 
            value=-1, dtype="int32")

  def _all_feats_categories(self):
    labels = [[y["feats"] for y in x] for x in self.data]
    all_cat = {}
    for x in labels:
      for y in x:
        a = y.split("|")
        for z in a: 
          b = z.split("=")
          if b[0] in all_cat.keys():
            pass
          else:
            all_cat[b[0]] = []
          if len(b) > 1:
            if b[1] not in all_cat[b[0]]:
              all_cat[b[0]].append(b[1])
    del all_cat["_"]
    all_cat = {k: v + ["[X]"] for k,v in all_cat.items()}
    all_cat["[X]"] = ["[X]"]
    return all_cat

  def create_feats_dict(self):
    all_cat = self._all_feats_categories()
    dict_feats_numb  = {}
    for n, (k,v) in enumerate(all_cat.items()):
      g = []
      g2 = dict(zip(v, range(1, len(v) + 1)))
      g = [n, g2]
      dict_feats_numb[k] = g
    dict_numb_feats = {}
    for k,v in dict_feats_numb.items():
      dict_numb_feats[v[0]] = [k, {v2: k2 for k2, v2 in v[1].items()}]
    Dataset.train_dicts["feats"] = {}
    Dataset.train_dicts["feats"]["type_num"] = dict_feats_numb
    Dataset.train_dicts["feats"]["num_type"] = dict_numb_feats
    # calculate lengths of feats
    num_feats = len(Dataset.train_dicts["feats"]["type_num"].keys())
    num_subfeats = set()
    for k, v in Dataset.train_dicts["feats"]["type_num"].items():
      g = list(v[1].values())
      if g:
        num_subfeats.add(max(g))
    num_subfeats = max(num_subfeats) + 1 # to account for 0
    Dataset.train_dicts["feats"]["length_feats"] = num_feats
    Dataset.train_dicts["feats"]["length_subfeats"] = num_subfeats

  def prepare_y_feats(self):
    labels = [[y["feats"] for y in x] for x in self.data]
    dict1 = Dataset.train_dicts["feats"]["type_num"]
    train_y_feats2 = np.zeros((len(self.data),
                     Dataset.train_max_sent_length, 
                     Dataset.train_dicts["feats"]["length_feats"]),
                     dtype="int32")
    for n0, x in enumerate(tqdm(labels)):
      for n1, y in enumerate(x):
        int_ = [0] * Dataset.train_dicts["feats"]["length_feats"]
        if y != "_":
          sep = y.split("|")
          for z in sep:
            sep2 = z.split("=")
            try:
              int_[dict1[sep2[0]][0]] = dict1[sep2[0]][1][sep2[1]]
            except:
              try:
                int_[dict1[sep2[0]][0]] = dict1[sep2[0]][1]["[X]"]
              except:
                int_[dict1["[X]"][0]] = dict1["[X]"][1]["[X]"]
        train_y_feats2[n0, n1] = int_
    self.feats = train_y_feats2
    print("### The data can be accessed at .feats ###")

  def create_tf_dataset(self, buffer_size=50, batch=10, shuffle=True):
    if self.task == "parser":
      x = tf.data.Dataset.from_tensor_slices((self.forms))
      y = tf.data.Dataset.from_tensor_slices((self.upos, self.feats, 
               self.deprel, self.head, self.lemmas, self.xpos))
      xy = tf.data.Dataset.zip((x,y))
      if shuffle==True:
        self.tf_dataset = xy.shuffle(buffer_size=buffer_size, 
                               reshuffle_each_iteration=True).batch(batch)
      else:
        self.tf_dataset = xy.batch(batch)
    elif self.task == "splitter":
      x = tf.data.Dataset.from_tensor_slices(self.or_text)
      y = tf.data.Dataset.from_tensor_slices((self.tokenized))
      xy = tf.data.Dataset.zip((x,y))
      if shuffle==True:
        self.tf_dataset = xy.shuffle(buffer_size=buffer_size, 
                               reshuffle_each_iteration=True).batch(batch)
      else:
        self.tf_dataset = xy.batch(batch)

  def _return_ufdx(self, pred, kind): # 0 and 2
    '''
    Args:
      preds: a numpy array containing the prediction for "upos" 
             or "deprel"
      kind: is "upos" or "deprel"

    Returns:
      list: a list of lists for the labels
    '''
    all_sents = []
    for s, sl in zip(pred,self.sent_length):
      sent = []
      for n2, t in enumerate(s):
        if n2 <= sl-1:
          label = Dataset.train_dicts[kind]["num_type"][t]
          sent.append(label)
      all_sents.append(sent)
    return all_sents

  def _return_feats(self, pred):
    '''
    Args:
      preds: a numpy array containing the prediction for "feats" 

    Returns:
      list: a list of lists for the labels
    '''
    all_sents = []
    for s, sl in zip(pred, self.sent_length):
      sent = []
      for n2, t in enumerate(s):
        if n2 <= sl-1:
          token = []
          for n3,f in enumerate(t):
            if f != 0:
              try:
                label = Dataset.train_dicts["feats"]["num_type"][n3][1][f]
                label = Dataset.train_dicts["feats"]["num_type"][n3][0] +\
                                                              "=" + label
              except:
                label = Dataset.train_dicts["feats"]["num_type"][10][1][1]
                label = Dataset.train_dicts["feats"]["num_type"][n3][0] +\
                                                              "=" + label
              token.append(label)
          if token == []: # this means all fields are 0. i.e, [0,0, ...]
            token = "_"
          else:
            token = "|".join(sorted(token))
          sent.append(token)
      all_sents.append(sent)
    return all_sents

  def _convert_predicted_dep_dist(self, value):
    '''
    Args:
      value: an integer representing the dep_distance value: an original
             negative value due to head_id - token_id
             was transformed into abs(value) + 
             Dataset.train_max_dep_distance)

    Returns:
      integer: the dep_distance value transformed into its original value
    '''
    if value <= Dataset.train_max_dep_distance:
      pass
    else:
      value = - (value - Dataset.train_max_dep_distance)
    return value

  def _return_heads2(self, pred):
    all_sents = []
    for s, sl in zip(pred, self.sent_length):
      sent = []
      for n2, t in enumerate(s):
        if n2 <= sl-1:
          sent.append(t)
      all_sents.append(sent)
    return all_sents

  def _return_heads(self, pred, chu=True):
    all_sents = []
    for s, sl in zip(pred, self.sent_length):
      sent = []
      if chu==True:
        ns = chuliu_edmonds.chuliu_edmonds(s[0:sl+1,0:sl+1])[1:]
      else:
        ns = np.argmax(s[0:sl+1,0:sl+1], axis=-1)[1:]
      for t in ns:
          sent.append(t)
      all_sents.append(sent)
    return all_sents

  def _return_dep_dist(self, pred):
    '''
    Args:
      preds: a numpy array containing the prediction for "dep_dist" 

    Returns:
      list: a list of lists for the dep_distance values
    '''
    all_sents = []
    for s, sl in zip(pred, self.sent_length):
      sent = []
      for n2, t in enumerate(s):
        if n2 <= sl-1:
          if t == 0:
            dep_dist = 0
          else:
            dep_dist = self._convert_predicted_dep_dist(t)
          sent.append(dep_dist)
      all_sents.append(sent)
    return all_sents

  def _return_lemma(self, pred):
    '''
    Args:
      preds: a numpy array containing the prediction for lemmas 

    Returns:
      list: a list of lists containing the lemmas
    '''
    all_sents = []
    for s, sl in zip(pred, self.sent_length):
      sent = []
      for n2, t in enumerate(s):
        if n2 <= sl-1:
          token = []
          for n3,f in enumerate(t):
            if f == Dataset.train_dicts["lemma"]["char_num"]["[end]"]:
              break
            else:
              label = Dataset.train_dicts["lemma"]["num_char"][f]
              token.append(label)
          token = "".join(token)
          sent.append(token)
      all_sents.append(sent)
    return all_sents

  def _build_tokens(self, uposs, deprels, featss, heads, lemmas, xpos):
    '''
    Args:
      uposs: a list of lists contains upos labels
      deprels: a list of lists contains deprel labels
      featss: a list of lists contains feats labels
      dep_dists: a list of lists contains the dep_distance values
      lemmas: a list of lists contains the lemmas

    Returns:
      list: a list of lists of dictionaries representing the tokens
    '''
    all_sents = []
    for a,b,c,d,e,f,z in zip(self.data, lemmas, uposs, featss, deprels,
                                                           heads, xpos):
      sent = []
      for n1, (g,h,i,l,m,n,o) in enumerate(zip(a,b,c,d,e,f,z)):
          token = {}
          token["id"] = n1 + 1 # encoded as number
          token["form"]   = g["form"]
          token["lemma"]  = h
          token["upos"]   = i
          token["feats"]  = l
          token["deprel"] = m
          token["head"]   = n
          token["xpos"] = o
          sent.append(token)
      all_sents.append(sent)
    return all_sents

  def _transform_predictions(self, preds, chu=True):
    ''''
    A wrapper function to get all predidictions

    Args:
      model = the Tensorflow model after training

    Returns:
      list: a list of lists of dictionaries representing the tokens
    '''
    uposs = self._return_ufdx(preds[0], "upos")
    deprels = self._return_ufdx(preds[2], "deprel")
    featss = self._return_ufdx(preds[1], "feats")
    heads = self._return_heads(preds[3], chu)
    lemmas = self._return_lemma(preds[4])
    xposs = self._return_ufdx(preds[5], "xpos")
    all_sents = self._build_tokens(uposs, deprels,featss, heads, lemmas,\
                                                                        xposs)
    return all_sents

  def _align_sentences(self, sent_predicted):
    ''''
    This aligns the sentences predicted with the original ones
    because the latter may have been
    split because of Dataset.train_max_sent_length

    Args:
      sent_predicted = a list of lists for the sentences

    Returns:
      list: a list of lists representing the sentences
    '''
    all_sents = []
    sent_id = None
    for x, s in zip(self.original_sent_ids, sent_predicted):
      if x == sent_id:
         ls = len(all_sents[x]) 
         all_sents[x] = all_sents[x] + s.copy()
         # renumber
         for n1, q in enumerate(all_sents[x]):
           q["id"] = n1 + 1
           #if n1 > ls - 1:
           #  if q["head"] == 0:
           #    q["head"] == random.choice(range(0, ls + 1)) 
           #  else:
           #    q["head"] = q["head"] + ls
      else:
        sent = s.copy()
        all_sents.append(sent)
      sent_id = x
    return all_sents

  def _align_tokens(self, sent_predicted):
    all_sents = []
    for n1, (s1, s2) in enumerate(zip(self.conllu, sent_predicted)):
      new_sent = []
      for t1 in s1:
        # the following is for MWE and elliptical nodes
        if "-" not in t1["id"] and "." not in t1["id"]: 
          tok = [x for x in s2 if x["id"] == int(t1["id"])][0]
          tok["sent_id"] = t1["sent_id"]
          new_sent.append(tok)
        else:
          new_sent.append(t1)
      all_sents.append(new_sent)
    return all_sents

  def _calculate_head(self, sent_predicted):
    for x in sent_predicted:
      for y in x:
          if str(y["id"]) != "0" and "-" not in str(y["id"]) and\
              "." not in str(y["id"]):
            if y["dep_dist"] == 0:
              y["head"] = 0
            else:
              y["head"] = y["dep_dist"] + y["id"]

  def return_predictions(self, preds, chu=True):
    preds = self._transform_predictions(preds, chu)
    preds = self._align_sentences(preds)
    preds = self._align_tokens(preds)
#    self._calculate_head(preds)
    return preds

  @staticmethod
  def transform_into_conllu(sent_predicted):
    all_sents = []
    # self.conllu allows to retrieve MWE
    for s in sent_predicted:
      sent = []
      # the following # is obligatory in v2
      sent_id = sent.append("# sent_id = " + s[0]["sent_id"])
      for to in s:
          token =(str(to["id"]), to["form"], to["lemma"],
                to["upos"],to["xpos"], to["feats"], str(to["head"]),
                to["deprel"], "_", "_")
          line = "\t".join(token)
          sent.append(line)
      join_sent = "\n".join(sent)
      all_sents.append(join_sent)
    all_sents = "\n\n".join(all_sents) + "\n"
    return all_sents

