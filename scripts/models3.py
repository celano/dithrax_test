# https://discuss.tensorflow.org/t/sharing-any-keras-model-on-the-hugging-face-hub/6950

import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers
from tensorflow.keras import regularizers

def dithrax_morphosyntax_train(max_sent_length, 
             max_position,
             max_word_length, 
             vocab_size, embed_dim, 
             lstm_dim, dropout,
             max_upos_num,
             num_feats,
             max_deprel_num,
             max_dep_dist,
             max_lemma_length,
             max_char_num_lemma,
             max_xpos_num):

    lstm_dim = ((lstm_dim // max_lemma_length) + 1) * max_lemma_length

    inputs = layers.Input(shape=(max_sent_length, max_word_length),
                          dtype=tf.int32)

    embeds = layers.Embedding(vocab_size, 
                             embed_dim, 
                             embeddings_regularizer=regularizers.l2(0.00001),
                             mask_zero=True) (inputs)

    y = layers.TimeDistributed(
                      layers.Bidirectional(
                        layers.LSTM(lstm_dim, 
                           kernel_regularizer=regularizers.l2(0.000001),
                           bias_regularizer=regularizers.l2(0.000001),
                           recurrent_regularizer=regularizers.l2(0.000001),
                           activity_regularizer=regularizers.l2(0.000001),
                           return_sequences=True) 
                                          )
                                    ) (embeds)
    y = layers.GaussianDropout(dropout)(y)
    y = layers.GaussianNoise(0.2)(y)

    x = layers.TimeDistributed(
                       layers.Bidirectional(
                        layers.LSTM(lstm_dim,
                             kernel_regularizer=regularizers.l2(0.000001),
                             bias_regularizer=regularizers.l2(0.000001),
                             recurrent_regularizer=regularizers.l2(0.000001),
                             activity_regularizer=regularizers.l2(0.000001)
                                   )
                                           )
                                    ) (y)
    x = layers.GaussianDropout(dropout)(x)
    x = layers.GaussianNoise(0.2)(x)

    mask = tf.reduce_any(tf.not_equal(inputs, 0), axis=-1)

    x = layers.Bidirectional(layers.LSTM(lstm_dim,
                              kernel_regularizer=regularizers.l2(0.000001),
                              bias_regularizer=regularizers.l2(0.000001),
                              recurrent_regularizer=regularizers.l2(0.000001),
                              activity_regularizer=regularizers.l2(0.000001),
                              return_sequences=True))(x, mask=mask)
    x = layers.GaussianDropout(dropout)(x)
    x = layers.GaussianNoise(0.2)(x)

#    x = layers.Bidirectional(layers.LSTM(lstm_dim,
#                              kernel_regularizer=regularizers.l2(0.000001),
#                              bias_regularizer=regularizers.l2(0.000001),
#                              recurrent_regularizer=regularizers.l2(0.000001),
#                              activity_regularizer=regularizers.l2(0.000001),
#                              return_sequences=True))(x, mask=mask)
#    x = layers.GaussianDropout(dropout)(x)
#    x = layers.GaussianNoise(0.2)(x)

#    x = layers.Bidirectional(layers.LSTM(lstm_dim,
#                              kernel_regularizer=regularizers.l2(0.000001),
#                              bias_regularizer=regularizers.l2(0.000001),
#                              recurrent_regularizer=regularizers.l2(0.000001),
#                              activity_regularizer=regularizers.l2(0.000001),
#                              return_sequences=True))(x, mask=mask)
#    x = layers.GaussianDropout(dropout)(x)
#    x = layers.GaussianNoise(0.2)(x)

    up =  layers.Dense(lstm_dim, activation="tanh")(x)
    up = layers.Dropout(dropout) (up)
    output_upos = layers.Dense(max_upos_num, 
                               activation="softmax", name ="upos") (up)


    of =  layers.Dense(lstm_dim, activation="tanh")(x)
    of = layers.Dropout(dropout) (of)
    of = layers.Concatenate()([of, output_upos])
    output_feats =  layers.Dense(num_feats, 
                                  activation="softmax", name ="feats") (of)

    da = layers.Dense(max_sent_length, activation="tanh") (x) #max_sent_length
    da = layers.Dropout(dropout) (da)
    yu = tf.ones((tf.shape(da)[0], 1, max_sent_length))
    yi = tf.ones((tf.shape(da)[0], max_sent_length + 1, 1))
    da = tf.concat([yu, da], 1)
    da = tf.concat([yi, da], -1)
    ones_ = tf.cast(tf.ones((tf.shape(mask)[0], 1)), "bool")
    mask_head = tf.concat([ones_, mask], 1)
    da = layers.Bidirectional(layers.LSTM(lstm_dim + 1,
                              kernel_regularizer=regularizers.l2(0.000001),
                              bias_regularizer=regularizers.l2(0.000001),
                              recurrent_regularizer=regularizers.l2(0.000001),
                              activity_regularizer=regularizers.l2(0.000001),
                              return_sequences=True))(da, mask=mask_head)

    da =  layers.Dense(max_sent_length + 1, activation="tanh") (da)
    da = layers.GaussianDropout(dropout)(da)
    da = layers.GaussianNoise(0.2)(da)


#    emask_head = tf.expand_dims(mask_head, 1)
#    emask_headT = tf.transpose(emask_head, perm=[0,2,1])
#    zeros = tf.zeros((tf.shape(da)[0], 
#                      max_sent_length+1, 
#                      max_sent_length+1)) 

#    da_masked = tf.where(emask_headT, da, zeros)
#    diag = tf.zeros((tf.shape(da_masked)[0], max_sent_length+1))
#    da = tf.linalg.set_diag(da_masked, diag)

    la = layers.Dense(max_sent_length + 1, activation="tanh") (da)
    la = layers.Dropout(0.5) (la)


    ha = layers.Dense(max_sent_length + 1, activation="tanh") (da)
    ha = layers.Dropout(0.5) (ha)

#    ha_masked = tf.where(emask_headT, ha, zeros)
#    ha = tf.linalg.set_diag(ha_masked, diag)

    cc = layers.Dot(axes=2)([la, ha])

    output_head = layers.Dense(max_sent_length + 1, 
                                   activation="softmax", name ="head") (cc)

    dr = layers.Dense(max_sent_length + 1, activation="tanh") (da)
    dr = layers.Dropout(0.5) (dr)

#    dr_masked = tf.where(emask_headT, dr, zeros)
#    dr = tf.linalg.set_diag(dr_masked, diag)

    zx = layers.Dense(max_sent_length + 1, activation="tanh") (da)
    zx = layers.Dropout(0.5) (zx)

#    zx_masked = tf.where(emask_headT, zx, zeros)
#    zx = tf.linalg.set_diag(zx_masked, diag)

    mx = layers.Lambda(lambda x: tf.transpose(x, perm=[0,2,1]))(zx)
    dotx = layers.Dot(axes=2)([output_head, mx])
    dotx = layers.Concatenate(axis=2)([dotx, dr])
    output_deprel = layers.Dense(max_deprel_num,
                        activation="softmax", name ="deprel") (dotx[:, 1:, :])

    l = layers.Reshape((max_sent_length, 
                        max_lemma_length, 
               (lstm_dim * 2)// max_lemma_length * max_word_length)) (y)
    l =  layers.Dense(lstm_dim, activation="tanh")(l)
    l = layers.Dropout(dropout) (l)
    output_lemma = layers.Dense(max_char_num_lemma, 
                                  activation="softmax", name ="lemma") (l)

    ox =  layers.Dense(lstm_dim, activation="tanh")(x)
    ox = layers.Dropout(dropout) (ox)
    output_xpos = layers.Dense(max_xpos_num, 
                                   activation="softmax", name ="xpos") (ox)

    model = keras.Model(inputs=[inputs], outputs=[output_upos,
                                                  output_feats,
                                                  output_deprel, 
                                                  output_head, 
                                                  output_lemma,
                                                  output_xpos
                                                 ])
    return model
