import tensorflow as tf
from dataset import Dataset
from models3 import dithrax_morphosyntax_train
from model_utils import Masked_CC_Loss,\
                        MaskedAccuracy,\
                        EarlyStoppingBestWeights
import numpy as np
from tqdm import tqdm

ud_train_p = "../normalized_texts/train.conllu"
ud_dev_p = "../normalized_texts/dev.conllu"

MAX_LENGTH=50
MAX_DEP_DISTANCE=50
BATCH=28

train = Dataset([ud_train_p], "train", "parser")
train.split_sents_add_dep_distance(max_length=MAX_LENGTH, 
                                   max_dep_distance=MAX_DEP_DISTANCE)
train.create_position_indeces()
train.create_char_dict("form")
train.split_words("form", max_word_length=20)
train.create_char_dict("lemma")
train.split_words("lemma", max_word_length=20)
train.create_label_dicts("upos")
train.prepare_y("upos")
train.create_label_dicts("deprel")
train.prepare_y("deprel")
train.prepare_y_dep_distance()
train.create_label_dicts("feats")
train.prepare_y("feats")
train.create_label_dicts("xpos")
train.prepare_y("xpos")
train.prepare_y_head()
train.create_tf_dataset(batch=BATCH, shuffle=False)

dev = Dataset([ud_dev_p], "dev", "parser")
dev.split_sents_add_dep_distance()
dev.create_position_indeces()
dev.split_words("form")
dev.split_words("lemma")
dev.prepare_y("upos")
dev.prepare_y("deprel")
dev.prepare_y("feats")
dev.prepare_y("xpos")
dev.prepare_y_head()
dev.create_tf_dataset(batch=BATCH, shuffle=False)

# model
model = dithrax_morphosyntax_train(train.train_max_sent_length, 
                     train.max_position,
                     train.train_max_form_length,
                     train.train_dicts["form"]["char_length"], 
                     1000, 800, 0.2,
                     train.train_dicts["upos"]["length"],
                     train.train_dicts["feats"]["length"],
                     train.train_dicts["deprel"]["length"],
                     train.train_max_dep_distance,
                     train.train_max_lemma_length,
                     train.train_dicts["lemma"]["char_length"],
                     train.train_dicts["xpos"]["length"])

model.summary()

early_stopping = tf.keras.callbacks.EarlyStopping(monitor='val_loss',
                                                  patience=2,
                                                  restore_best_weights=True)

esbw = EarlyStoppingBestWeights(patience=50, output_number=6)

learning_rate = tf.keras.optimizers.schedules.PiecewiseConstantDecay(
    [15000, 27000], [0.001, 0.0001, 0.00007])

opt = tf.keras.optimizers.Adam(learning_rate=learning_rate,
      clipvalue=4.5, beta_1=0.9, beta_2=0.9, weight_decay=1e-4)

model.compile(optimizer=opt,
    loss={"upos" : Masked_CC_Loss(mask_value=-1),
          "feats": Masked_CC_Loss(mask_value=-1),
          "deprel" : Masked_CC_Loss(mask_value=-1),
          "head" : Masked_CC_Loss(mask_value=-1), 
          "lemma": Masked_CC_Loss(mask_value=0),
          "xpos": Masked_CC_Loss(mask_value=-1)

         },
    metrics={"upos" : [MaskedAccuracy(mask_value=-1)],
             "feats": [MaskedAccuracy(mask_value=-1)],
             "deprel": [MaskedAccuracy(mask_value=-1)],
             "head": [MaskedAccuracy(mask_value=-1)],
             "lemma": [MaskedAccuracy(mask_value=0)],
             "xpos": [MaskedAccuracy(mask_value=-1)]
            },

            )
history = model.fit(train.tf_dataset, epochs=1, 
                    validation_data=(dev.tf_dataset), 
                    callbacks=[esbw], verbose=1)

model.save_weights("mymodel.weights.h5")
print("models has been saved as mymodel.weights.h5")
