import tensorflow as tf
from tensorflow import keras
import numpy as np

#@keras.saving.register_keras_serializable()
class Masked_CC_Loss(tf.keras.losses.Loss):
   def __init__(self, mask_value=0, **kwargs):
      super().__init__(**kwargs)
      self.mask_value = mask_value
      self.keras_loss = tf.keras.losses.SparseCategoricalCrossentropy()

   def __call__(self, y_true, y_pred, sample_weight=None):
      y_true_masked = tf.boolean_mask(y_true, tf.not_equal(y_true,
                                                           self.mask_value))
      y_pred_masked = tf.boolean_mask(y_pred, tf.not_equal(y_true, 
                                                           self.mask_value))
      return self.keras_loss(y_true_masked, y_pred_masked)

   def get_config(self):
       config = super().get_config()
       return {**config}

#@keras.saving.register_keras_serializable()
class MaskedAccuracy(tf.keras.metrics.Metric):
   def __init__(self, mask_value=0, **kwargs):
      super().__init__(**kwargs)
      self.mask_value = mask_value
      self.keras_accuracy = tf.keras.metrics.SparseCategoricalAccuracy()

   def update_state(self, y_true, y_pred, sample_weight=None):
      y_true_masked = tf.boolean_mask(y_true, tf.not_equal(y_true, 
                                                             self.mask_value))
      y_pred_masked = tf.boolean_mask(y_pred, tf.not_equal(y_true, 
                                                             self.mask_value))
      self.keras_accuracy.update_state(y_true_masked, y_pred_masked)

   def result(self):
       return self.keras_accuracy.result()

   def get_config(self):
       config = super().get_config()
       return {**config}

#@keras.saving.register_keras_serializable()
class EarlyStoppingBestWeights(keras.callbacks.Callback):
    def __init__(self, patience=3, output_number=6):
        super().__init__()
        self.output_number = output_number
        self.patience = patience
        self.best_weights = None

    def on_train_begin(self, logs=None):
        self.wait = 0
        self.stopped_epoch = 0
        self.best = 0

    def on_epoch_end(self, epoch, logs=None):
        current = 0
        output_number = 0
        for k,v in logs.items():
          if "val" in k and "accuracy" in k:
            current += v
        current = current / self.output_number
        print("\nThe mean for val accuracies is {:.4f}\n".format(current))
        if np.less(self.best, current):
            self.best = current
            self.best_weights = self.model.get_weights()
        else:
            self.wait += 1
            print("\nWait score is {}\n".format(self.wait))
            if self.wait >= self.patience:
                self.stopped_epoch = epoch
                self.model.stop_training = True
                print("Restoring model weights from best epoch.")
                self.model.set_weights(self.best_weights)

    def on_train_end(self, logs=None):
        if self.stopped_epoch > 0:
            print("Epoch {}: early stopping".format(self.stopped_epoch + 1))

    def get_config(self):
       config = super().get_config()
       return {**config}


